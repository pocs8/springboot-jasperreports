package br.com.wostecnologia.jasperreports.controller;

import br.com.wostecnologia.jasperreports.repository.ProductRepository;
import br.com.wostecnologia.jasperreports.service.ProductService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by wesleyosantos91 in 23/09/18
 */
@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService service;

    @Autowired
    private ProductRepository repository;


    @GetMapping("/report")
    public void printReportPdf(HttpServletResponse response) throws JRException, IOException, SQLException {
        service.printReportPdf(response);
    }

    @GetMapping("/report/docx")
    public void printReportDocx(HttpServletResponse response) throws JRException, IOException, SQLException {

       service.printReportDocx(response);

    }

    @GetMapping("/report/xls")
    public void printReportXls(HttpServletResponse response) throws JRException, IOException, SQLException {

        service.printReportXls(response);
    }
}