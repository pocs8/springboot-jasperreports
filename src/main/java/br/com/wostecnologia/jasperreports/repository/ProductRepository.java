package br.com.wostecnologia.jasperreports.repository;

import br.com.wostecnologia.jasperreports.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by wesleyosantos91 in 23/09/18
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
}
