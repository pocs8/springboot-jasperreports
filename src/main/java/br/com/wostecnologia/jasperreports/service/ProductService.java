package br.com.wostecnologia.jasperreports.service;

import br.com.wostecnologia.jasperreports.model.Product;
import br.com.wostecnologia.jasperreports.repository.ProductRepository;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by wesleyosantos91 in 23/09/18
 */
@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    @Autowired
    private RelatorioService relatorioService;

    @Value("${caminho.relatorio.product}")
    private String caminhoArquivo;

    public void printReportPdf(HttpServletResponse response) throws JRException, IOException, SQLException {

        final JasperReport jasperReport = relatorioService.carregarRelatorio(caminhoArquivo);

        Map<String, Object> params = new HashMap<>();

        final List<Product> all = repository.findAll();

        final JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(all);

        relatorioService.exportarParaPDFStream(jasperReport, params, jrBeanCollectionDataSource, response);
    }

    public void printReportDocx(HttpServletResponse response) throws JRException, IOException, SQLException {

        final JasperReport jasperReport = relatorioService.carregarRelatorio(caminhoArquivo);

        Map<String, Object> params = new HashMap<>();

        final List<Product> all = repository.findAll();

        final JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(all);

        relatorioService.exportarParaDoc(jasperReport, params, jrBeanCollectionDataSource, response);

    }

    public void printReportXls(HttpServletResponse response) throws JRException, IOException, SQLException {

        final JasperReport jasperReport = relatorioService.carregarRelatorio(caminhoArquivo);

        Map<String, Object> params = new HashMap<>();

        final List<Product> all = repository.findAll();

        final JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(all);

        relatorioService.exportarParaExcel(jasperReport, params, jrBeanCollectionDataSource, response);

    }
}
