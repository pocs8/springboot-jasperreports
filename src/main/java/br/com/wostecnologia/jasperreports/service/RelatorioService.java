package br.com.wostecnologia.jasperreports.service;

import br.com.wostecnologia.jasperreports.exception.GenericException;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleDocxReportConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Objects;

/**
 * Created by wesleyosantos91 in 23/09/18
 */
@Service
@Slf4j
public class RelatorioService {

    private final String ERRO_RELATORIO = "Não foi possível carregar o relatório!";
    private final String ERRO_RELATORIO_LOG = "Erro ao gerar relatório: {}";

    /**
     * Método responsável por carregar o relátorio .jrxml e converter para um .jasper
     *
     * @param caminhoArquivo
     * @Return
     *
     * @author Wesley Oliveira - 23/09/2018
     */
    public JasperReport carregarRelatorio(String caminhoArquivo) {

        try {
            InputStream reportStream = this.getClass().getResourceAsStream(caminhoArquivo);

            JasperDesign jasperDesign = JRXmlLoader.load(reportStream);

            return JasperCompileManager.compileReport(jasperDesign);

        } catch (JRException ex) {

            log.error(ERRO_RELATORIO, ex);
            throw new GenericException("Não foi possével exportar o relatório causa {} " + ex.getCause());
        }
    }

    /**
     * Método responsável por exportar o PDF para um stream
     *
     * @param jasperReport
     * @param params
     * @param dataSource
     * @param response
     *
     * @author Wesley Oliveira - 23/09/2018
     */
    public void exportarParaPDFStream(JasperReport jasperReport, Map<String, Object> params, JRDataSource dataSource, HttpServletResponse response) {

        try {

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);

            response.setContentType("application/pdf");
            // para fazer download do relatório troque 'inline' por 'attachment'
            response.setHeader("Content-Disposition", "inline; filename=products.pdf");

            OutputStream outputStream = response.getOutputStream();

            if (Objects.nonNull(response) && Objects.nonNull(outputStream)) {

                JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
            }

        } catch (JRException | IOException ex) {

            log.error(ERRO_RELATORIO_LOG, ex);
            throw new GenericException("Não foi possível exportar o relatório causa {} " + ex.getCause());
        }
    }

    /**
     * Método responsável por exportar o arquivo para um excel
     *
     * @param jasperReport
     * @param params
     * @param dataSource
     * @param response
     *
     * @author Wesley Oliveira - 23/09/2018
     */
    public void exportarParaExcel(JasperReport jasperReport, Map<String, Object> params, JRDataSource dataSource, HttpServletResponse response) {

        try {

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);

            response.setContentType("application/xls");
            // para fazer download do relatório troque 'inline' por 'attachment'
            response.setHeader("Content-Disposition", "inline; filename=products.xls");

            OutputStream outputStream = response.getOutputStream();

            JRXlsxExporter exporter = new JRXlsxExporter();

            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));

            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setCellHidden(true);
            configuration.setShrinkToFit(true);
            configuration.setIgnorePageMargins(true);
            configuration.setFontSizeFixEnabled(true);
            configuration.setRemoveEmptySpaceBetweenRows(true);
            configuration.setRemoveEmptySpaceBetweenColumns(true);
            exporter.setConfiguration(configuration);

            if (Objects.nonNull(outputStream)) {
                exporter.exportReport();
            }

        } catch (JRException | IOException ex) {

            log.error(ERRO_RELATORIO_LOG, ex);
            throw new GenericException("Não foi possível exportar o relatório causa {} " + ex.getCause());
        }
    }

    /**
     * Método responsável por exportar o arquivo para um doc
     *
     * @param jasperReport
     * @param params
     * @param dataSource
     * @param response
     *
     * @author Wesley Oliveira - 23/09/2018
     */
    public void exportarParaDoc(JasperReport jasperReport, Map<String, Object> params, JRDataSource dataSource, HttpServletResponse response) {

        try {

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);

            response.setContentType("application/docx");
            // para fazer download do relatório troque 'inline' por 'attachment'
            response.setHeader("Content-Disposition", "inline; filename=products.docx");

            OutputStream outputStream = response.getOutputStream();

            JRDocxExporter exporter = new JRDocxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(response.getOutputStream()));

            SimpleDocxReportConfiguration config = new SimpleDocxReportConfiguration();
            config.setFlexibleRowHeight(true); //Set desired configuration

            exporter.setConfiguration(config);

            if (Objects.nonNull(outputStream)) {
                exporter.exportReport();
            }

        } catch (JRException | IOException ex) {

            log.error(ERRO_RELATORIO_LOG, ex);
            throw new GenericException("Não foi possível exportar o relatório causa {} " + ex.getCause());
        }
    }
}