package br.com.wostecnologia.jasperreports.exception;

/**
 * Created by wesleyosantos91 in 23/09/18
 */
public class GenericException extends RuntimeException {

    public GenericException(String message) {
        super(message);
    }
}
